import json
import socket
import sys


class NoobBot(object):


    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.speed = 0.01

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        #return self.msg("join",{"name":self.name,"key":self.key})
        return self.msg("join",{"name":self.name,"key":self.key})

    def throttle(self, throttle):
        if self.angle > 40:
            throttle = 0
        if throttle == 1:
            if self.speed > 8:
                self.speed += 0.03
            elif self.speed > 5:
                self.speed += 0.07
            elif self.speed > 3:
                self.speed += 0.11
            else:
                self.speed += 0.15
        else:
            self.speed -=  0.15
        self.msg("throttle", throttle)
        print self.speed

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.started = True
        self.ping()

    def on_car_positions(self, data):
        i = 0
        while 1:
            if data[i]['id']['name'] == 'Nike':
                self.carId = i
                self.thisIndex = data[i]['piecePosition']['pieceIndex']
                break
            else:
                i += 1
        self.angle = data[self.carId]['angle']
        self.calculate_pieces(data)
        print int(data[self.carId]['piecePosition']['inPieceDistance'])+ self.thisIndex * 100
        if 'angle' in self.thisPiece:
            self.throttle_in_angle(data)
        else:
            self.throttle_expecting_angle(data)

    def throttle_in_angle(self, data):
        #if 'angle' in self.nextPiece and self.nextPiece['angle'] * self.thisPiece['angle'] < 0 and self.angle < 30:
        #    self.throttle(1)
        if self.speed >  (100 - ((100/self.thisPiece['radius']) * abs(self.thisPiece['angle'])))/10:
            if data[self.carId]['piecePosition']['inPieceDistance'] / self.thisPiece['radius'] > 0.5 and self.angle < 20:
                self.throttle(1)
            else:
                self.throttle(0)
        elif 'angle' in self.nextPiece:
            if self.speed > min((100 - ((100/self.nextPiece['radius']) * abs(self.nextPiece['angle'])))/10 , 5):
                self.throttle(0)
            else:
                self.throttle(1)
        else:
            self.throttle(1)

    def throttle_expecting_angle(self, data):
        distance_left, nextPiece, nextPieceIndex = self.distance_to_next_angle(data)
        if self.speed - (distance_left/self.speed )* 0.30 >  (100 - ((100/nextPiece['radius']) * abs(nextPiece['angle'])))/10:
            self.throttle(0)
        else:
            self.throttle(1)

    def distance_to_next_angle(self, data):
        distance = 0
        distance += self.thisPiece['length'] - int(data[self.carId]['piecePosition']['inPieceDistance'])
        thisIndex = self.thisIndex
        inLoop = True
        while inLoop:
            thisIndex = int(thisIndex + 1)
            if len(self.pieces) >  thisIndex:
                nextPiece = self.pieces[thisIndex]
            else:
                nextPiece = self.pieces[0]
                thisIndex = 0
            if 'angle' in nextPiece:
                return distance, nextPiece, thisIndex
                inLoop = False
            else:
                distance += nextPiece['length']



    def calculate_pieces(self,data):
        thisIndex = int(data[self.carId]['piecePosition']['pieceIndex'])
        self.thisPiece = self.pieces[thisIndex]
        if len(self.pieces) > thisIndex + 1 :
            self.nextPiece = self.pieces[thisIndex + 1]
        else:
            self.nextPiece = self.pieces[0]

    def on_crash(self, data):
        print("Someone crashed")
        print self.thisPiece
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
    	print ("Got pieces")
    	self.pieces = data['race']['track']['pieces']
    	print (self.pieces)
    	self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit':self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
